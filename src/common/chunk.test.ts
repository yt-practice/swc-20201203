import { chunk, chunkIterable } from './chunk'
import { range, rangeIterable } from './range'

describe('chunk', () => {
	test('sample1', () => {
		expect(chunk(range(10, 1), 3)).toEqual([
			[1, 2, 3],
			[4, 5, 6],
			[7, 8, 9],
			[10],
		])
	})

	test('sample2', () => {
		expect(chunk(range(10, 1), 4)).toEqual([
			[1, 2, 3, 4],
			[5, 6, 7, 8],
			[9, 10],
		])
	})
})

describe('chunkIterable', () => {
	test('sample1', async () => {
		const iter = chunkIterable(rangeIterable(10, 1), 3)
		expect(await iter.next()).toEqual({ done: false, value: [1, 2, 3] })
		expect(await iter.next()).toEqual({ done: false, value: [4, 5, 6] })
		expect(await iter.next()).toEqual({ done: false, value: [7, 8, 9] })
		expect(await iter.next()).toEqual({ done: false, value: [10] })
		expect(await iter.next()).toEqual({ done: true, value: undefined })
	})

	test('sample2', async () => {
		const iter = chunkIterable(rangeIterable(10, 1), 4)
		expect(await iter.next()).toEqual({ done: false, value: [1, 2, 3, 4] })
		expect(await iter.next()).toEqual({ done: false, value: [5, 6, 7, 8] })
		expect(await iter.next()).toEqual({ done: false, value: [9, 10] })
		expect(await iter.next()).toEqual({ done: true, value: undefined })
	})
})
