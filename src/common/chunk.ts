export const chunk = <T>(array: T[], size: number) => {
	if (size < 1) throw new TypeError('size must be greater than 0.')
	const chunked: T[][] = []

	for (let index = 0; index < array.length; index += size) {
		chunked.push(array.slice(index, index + size))
	}

	return chunked
}

export const chunkIterable = async function* <T>(
	iterable: AsyncIterable<T>,
	size: number,
) {
	if (size < 1) throw new TypeError('size must be greater than 0.')
	let list: T[] = []
	for await (const item of iterable) {
		list.push(item)
		if (list.length < size) continue
		yield list
		list = []
	}
	if (list.length) yield list
}
