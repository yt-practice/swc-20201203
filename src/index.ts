import { chunkIterable } from './common/chunk'

export const main = async () => {
	// pass
	console.log('hoge')
	const iternums = async function* () {
		for (let i = 0; i < 10; ++i) yield i
	}
	for await (const nums of chunkIterable(iternums(), 3)) {
		console.log({ nums })
	}
	console.log('end')
}

main().catch(x => {
	console.log('# something happens.')
	console.error(x)
	if ('undefined' === typeof process) return
	process.exit(1)
})
